const csv = require('csvtojson')
const fs = require("fs")


function convert(csvFilePath, writeFilePath) {
    csv()
        .fromFile(csvFilePath)
        .then((jsonObj) => {
            const jsonString = JSON.stringify(jsonObj, null, 2);
            fs.writeFile(writeFilePath, jsonString, (err) => {
                if (err) {
                    console.log(err)
                }
            })
        })
}


//Converting CSV to JSON

convert(__dirname + "/matches.csv", __dirname + "/matches.json")
convert(__dirname + "/deliveries.csv", __dirname + "/deliveries.json")