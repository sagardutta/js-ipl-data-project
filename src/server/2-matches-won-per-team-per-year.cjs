const fs = require("fs")

//Reading the matches.json file
fs.readFile("src/data/matches.json", 'utf-8', function (err, data) {

    if (err) {
        throw err
    }

    //Parsing obtained data
    var matches = JSON.parse(data);

    let matches_won_per_team = {}

    matches.map((match) => {

        if (matches_won_per_team[match.season]) {
            if (matches_won_per_team[match.season][match.winner]) {
                matches_won_per_team[match.season][match.winner] = matches_won_per_team[match.season][match.winner] + 1
            } else {
                matches_won_per_team[match.season][match.winner] = 1
            }
        } else {
            matches_won_per_team[match.season] = { [match.winner]: 1 }
        }
    })

    console.log(matches_won_per_team)

    const writeFilePath = "src/public/2-matches-won-per-team-per-year.json"

    fs.writeFile(writeFilePath, JSON.stringify(matches_won_per_team, null, 2), (err) => {
        if (err) {
            throw err
        }
    })

});