const fs = require("fs");

function getMatchIDs() {

    //Reading the matches.json file
    return new Promise((resolve, reject) => {
        fs.readFile("src/data/matches.json", 'utf-8', function (err, data) {

            if (err) {
                throw err
            }

            //Parsing obtained data
            var matches = JSON.parse(data)

            let match_ids_2015 = []

            //Getting match IDs of 2015
            matches.forEach(match => {
                if (match.season == "2015") {
                    match_ids_2015.push(match.id)
                }
            });
            resolve(match_ids_2015);
        });
    })

}

function getEconomicalBowlers(match_ids_2015) {

    return new Promise((resolve, reject) => {
        //Reading the deliveries.json file
        fs.readFile("src/data/deliveries.json", 'utf-8', function (err, data) {

            if (err) {
                throw err
            }

            //Parsing obtained data
            var deliveries = JSON.parse(data);

            let bowler_runs_and_balls = {}

            //Extract and store runs and no. of balls for each bowler
            deliveries.forEach(delivery => {

                if (match_ids_2015.includes(delivery.match_id)) {
                    if (bowler_runs_and_balls[delivery.bowler]) {
                        bowler_runs_and_balls[delivery.bowler]["runs"] += parseInt(delivery.total_runs) + parseInt(delivery.extra_runs)
                        bowler_runs_and_balls[delivery.bowler]["balls"] += 1

                    } else {
                        bowler_runs_and_balls[delivery.bowler] = {
                            runs: parseInt(delivery.total_runs) + parseInt(delivery.extra_runs),
                            balls: 1
                        }
                    }
                }
            });

            // console.log(bowler_runs_and_balls)
            
            let economical_bowlers = {}

            //Calculate economy
            Object.keys(bowler_runs_and_balls).forEach(bowler => {
                economical_bowlers[bowler] = parseFloat((bowler_runs_and_balls[bowler]["runs"] / (bowler_runs_and_balls[bowler]["balls"] / 6)).toFixed(2))
            })

            const sortedBowlers = Object.entries(economical_bowlers).sort((a,b)=>b[1]-a[1])

            console.log(sortedBowlers)
            resolve(sortedBowlers.slice(0,10))
        });
    })

}

function writefile(sortedBowlers) {

    console.log("writing file")

    const writeFilePath = "src/public/4-top-10-economical-bowlers-in-2015.json"

    fs.writeFile(writeFilePath, JSON.stringify(sortedBowlers, null, 2), (err) => {

        if (err) {
            throw err
        }
    })
}

//Promise chaining
getMatchIDs()
    .then((match_ids_2015) => getEconomicalBowlers(match_ids_2015))
    .then((sortedBowlers) => writefile(sortedBowlers));
