const fs = require("fs");

function getMatchIDs() {

    //Reading the matches.json file
    return new Promise((resolve, reject) => {
        fs.readFile("src/data/matches.json", 'utf-8', function (err, data) {

            if (err) {
                throw err
            }

            //Parsing obtained data
            var matches = JSON.parse(data)

            let match_ids_2016 = []

            matches.forEach(match => {
                if (match.season == "2016") {
                    match_ids_2016.push(match.id)
                }
            });
            resolve(match_ids_2016);
        });
    })

}

function getExtraRuns(match_ids_2016) {

    return new Promise((resolve, reject) => {
        //Reading the deliveries.json file
        fs.readFile("src/data/deliveries.json", 'utf-8', function (err, data) {

            if (err) {
                throw err
            }

            //Parsing obtained data
            var deliveries = JSON.parse(data);

            let extra_runs = {}

            deliveries.forEach(delivery => {

                if (match_ids_2016.includes(delivery.match_id)) {
                    if (extra_runs[delivery.batting_team]) {
                        extra_runs[delivery.batting_team] = extra_runs[delivery.batting_team] + parseInt(delivery.extra_runs)
                    } else {
                        extra_runs[delivery.batting_team] = parseInt(delivery.extra_runs)
                    }
                }
            });

            console.log(extra_runs)
            resolve(extra_runs)
        });
    })

}

function writefile(extra_runs) {

    console.log("writing file")

    const writeFilePath = "src/public/3-extra-runs-conceded-per-team-in-2016.json"

    fs.writeFile(writeFilePath, JSON.stringify(extra_runs, null, 2), (err) => {

        if (err) {
            throw err
        }
    })
}

//Promise chaining
getMatchIDs()
    .then((match_ids_2016) => getExtraRuns(match_ids_2016))
    .then((extra_runs) => writefile(extra_runs));
