const fs = require("fs")

//Reading the matches.json file
fs.readFile("src/data/matches.json", 'utf-8', function (err, data) {

    if (err) {
        throw err
    }

    //Parsing obtained data
    var matches = JSON.parse(data);

    let player_of_match = {}

    //Extracted all Player_of_match for each season
    matches.map((match) => {
        if (player_of_match[match.season]) {
            if (player_of_match[match.season][match.player_of_match]) {
                player_of_match[match.season][match.player_of_match] += 1
            } else {
                player_of_match[match.season][match.player_of_match] = 1
            }
        } else {
            player_of_match[match.season] = {
                [match.player_of_match]: 1
            }
        }
    })

    // console.log(player_of_match)

    let max_player_of_match = {}

    Object.keys(player_of_match).forEach(season => {

        //Sorted by awards descending, then sliced to obtain highest winner
        let best_player_of_season = Object.entries(player_of_match[season]).sort((a, b) => b[1] - a[1]).slice(0, 1)
        max_player_of_match[season] = best_player_of_season.flat()
    });

    console.log(max_player_of_match)

    const writeFilePath = "src/public/6-players-who-won-the-highest-number-of-player-of-the-match-for-each-season.json"

    fs.writeFile(writeFilePath, JSON.stringify(max_player_of_match, null, 2), (err) => {
        if (err) {
            throw err
        }
    })

});