const fs = require("fs")

//Reading the matches.json file
fs.readFile("src/data/matches.json", 'utf-8', function (err, data) {
    
    if (err){
        throw err
    }

    //Parsing obtained data
    var matches = JSON.parse(data);

    let matches_per_year = {}

    matches.map((match)=>{

        if(matches_per_year[match.season]){
            matches_per_year[match.season]=matches_per_year[match.season]+1
        }else{
            matches_per_year[match.season]=1
        }
    })

    console.log(matches_per_year)

    const writeFilePath = "src/public/1-matches-per-year.json"

    fs.writeFile(writeFilePath, JSON.stringify(matches_per_year, null, 2), (err) => {
        if (err) {
            throw err
        }
    })

});




// var matchesReadStream = fs.createReadStream("/home/sagar/MountBlue/js_ipl_project/src/data/matches.json", 'utf8')
 
// matchesReadStream.on('data', function(chunk){

//     console.log("\n")
//     console.log(chunk)
// }