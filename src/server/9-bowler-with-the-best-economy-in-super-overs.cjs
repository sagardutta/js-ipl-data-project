const fs = require("fs")

//Reading the deliveries.json file
fs.readFile("src/data/deliveries.json", 'utf-8', function (err, data) {

    if (err) {
        throw err
    }

    //Parsing obtained data
    var deliveries = JSON.parse(data);

    let bowler_record = {}

    //Extracting bowler name, runs and balls in super over
    deliveries.map((delivery) => {
        if (delivery.is_super_over != "0") {
            if (bowler_record[delivery.bowler]) {
                bowler_record[delivery.bowler]["runs"] += parseInt(delivery.total_runs)
                bowler_record[delivery.bowler]["balls"] += 1
            } else {
                bowler_record[delivery.bowler] = {
                    runs: parseInt(delivery.total_runs),
                    balls: 1
                }
            }
        }
    })

    // console.log(bowler_record)

    let bowler_with_highest_economy = ""
    let economy = 0

    //Comparing highest economy
    Object.entries(bowler_record).forEach((bowler) => {
        //bowler[0] is bowler name
        //bowler[1] is {runs, balls}

        let current_economy = parseFloat(bowler[1].runs / (bowler[1].balls / 6))

        if (current_economy > economy) {
            economy = current_economy
            bowler_with_highest_economy = bowler[0]
        }

    })

    let req_data = `Bowler with highest economy in super over is ${bowler_with_highest_economy}, economy is ${economy}`

    const writeFilePath = "src/public/9-bowler-with-the-best-economy-in-super-overs.json"

    fs.writeFile(writeFilePath, JSON.stringify(req_data, null, 2), (err) => {
        if (err) {
            throw err
        }
    })

});