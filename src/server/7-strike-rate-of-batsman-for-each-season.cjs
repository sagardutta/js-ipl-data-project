const fs = require("fs");

function getMatchIDs() {

    //Reading the matches.json file
    return new Promise((resolve, reject) => {
        fs.readFile("src/data/matches.json", 'utf-8', function (err, data) {

            if (err) {
                throw err
            }

            //Parsing obtained data
            var matches = JSON.parse(data)

            let match_ids_and_seasons = {}

            //Getting match IDs and respective Seasons
            matches.forEach(match => {
                match_ids_and_seasons[match.id] = match.season
            });

            // console.log(match_ids_and_seasons)

            resolve(match_ids_and_seasons);
        });
    })

}

function getEconomicalBowlers(match_ids_and_seasons) {

    return new Promise((resolve, reject) => {
        //Reading the deliveries.json file
        fs.readFile("src/data/deliveries.json", 'utf-8', function (err, data) {

            if (err) {
                throw err
            }

            //Parsing obtained data
            var deliveries = JSON.parse(data);

            let batsman_balls_and_runs = {}

            //Extract and store runs and no. of balls for each batsman
            deliveries.forEach(delivery => {

                let season = match_ids_and_seasons[delivery.match_id]

                if (batsman_balls_and_runs[season]) {
                    if (batsman_balls_and_runs[season][delivery.batsman]) {
                        batsman_balls_and_runs[season][delivery.batsman]["balls"] += 1
                        batsman_balls_and_runs[season][delivery.batsman]["runs"] += parseInt(delivery.batsman_runs)

                    } else {
                        batsman_balls_and_runs[season][delivery.batsman] = {
                            balls: 1,
                            runs: parseInt(delivery.batsman_runs)
                        }
                    }
                } else {
                    batsman_balls_and_runs[season] = {
                        [delivery.batsman]: {
                            balls: 1,
                            runs: parseInt(delivery.batsman_runs)
                        }
                    }
                }

            });

            // console.log(batsman_balls_and_runs)

            let strike_rate = {}

            //Calculating strike rate
            Object.entries(batsman_balls_and_runs).forEach((season) => {

                let year = season[0]//season[0] is year
                //season[1] is batsman list in that season

                Object.entries(season[1]).forEach((batsman) => {

                    let batsman_name = batsman[0]//batsman[0] is batsman name
                    //batsman[1] is {balls:xx,runs:yy}
                    let balls = batsman[1].balls
                    let runs = batsman[1].runs

                    if (strike_rate[year]) {
                        strike_rate[year][batsman_name] = Math.round((runs / balls) * 100)
                    } else {
                        strike_rate[year] = {
                            [batsman_name]: Math.round((runs / balls) * 100)
                        }

                    }
                })
            })

            // console.log(strike_rate)

            resolve(strike_rate)
        });
    })

}

function writefile(strike_rate) {

    console.log("writing file")

    const writeFilePath = "src/public/7-strike-rate-of-batsman-for-each-season.json"

    fs.writeFile(writeFilePath, JSON.stringify(strike_rate, null, 2), (err) => {

        if (err) {
            throw err
        }
    })
}

//Promise chaining
getMatchIDs()
    .then((match_ids_and_seasons) => getEconomicalBowlers(match_ids_and_seasons))
    .then((strike_rate) => writefile(strike_rate));
