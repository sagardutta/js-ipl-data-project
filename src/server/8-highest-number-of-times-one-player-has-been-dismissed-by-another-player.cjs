const fs = require("fs")

//Reading the deliveries.json file
fs.readFile("src/data/deliveries.json", 'utf-8', function (err, data) {

    if (err) {
        throw err
    }

    //Parsing obtained data
    var deliveries = JSON.parse(data);

    let player_dismissed = {}

    //Extracting Batsman, the bowler who dismissed him, no. of times
    deliveries.map((delivery) => {

        if (delivery.player_dismissed.length > 0) {
            if (player_dismissed[delivery.batsman]) {
                if (player_dismissed[delivery.batsman][delivery.bowler]) {
                    player_dismissed[delivery.batsman][delivery.bowler] += 1
                } else {
                    player_dismissed[delivery.batsman][delivery.bowler] = 1
                }
            } else {
                player_dismissed[delivery.batsman] = {
                    [delivery.bowler]: 1
                }
            }
        }
    })

    // console.log(player_dismissed)

    let batsman_dismissed = ""
    let bowler_who_dismissed = ""
    let count = 0

    //Geting the highest no. of dismissed
    Object.entries(player_dismissed).forEach((batsman_record) => {
        batsman_name = batsman_record[0]
        Object.entries(batsman_record[1]).forEach((bowler_record) => {
            // console.log(bowler_record)
            let bowler_name = bowler_record[0]
            let score = bowler_record[1]

            if (score > count) {
                batsman_dismissed = batsman_name
                bowler_who_dismissed = bowler_name
                count = score
            }
        })
    })

    let data_record = `${batsman_dismissed} was dismissed by ${bowler_who_dismissed} ${count} times`

    const writeFilePath = "src/public/8-highest-number-of-times-one-player-has-been-dismissed-by-another-player.json"

    fs.writeFile(writeFilePath, JSON.stringify(data_record, null, 2), (err) => {
        if (err) {
            throw err
        }
    })

});