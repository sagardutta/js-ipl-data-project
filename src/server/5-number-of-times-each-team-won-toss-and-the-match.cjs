const fs = require("fs")

//Reading the matches.json file
fs.readFile("src/data/matches.json", 'utf-8', function (err, data) {

    if (err) {
        throw err
    }

    //Parsing obtained data
    var matches = JSON.parse(data);

    let match_and_toss_winner = {}

    matches.map((match) => {

        if (match.winner == match.toss_winner) {
            if (match_and_toss_winner[match.winner]) {
                match_and_toss_winner[match.winner] += 1
            } else {
                match_and_toss_winner[match.winner] = 1
            }
        }

    })

    console.log(match_and_toss_winner)

    const writeFilePath = "src/public/5-number-of-times-each-team-won-toss-and-the-match.json"

    fs.writeFile(writeFilePath, JSON.stringify(match_and_toss_winner, null, 2), (err) => {
        if (err) {
            throw err
        }
    })

});