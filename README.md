# JS IPL Data Project

<br>

## Description

<br>

This project intends to show some interesting analysis on IPL data from last few years, using javascript.

<br>

**List of reports:**

1. Number of matches played per year for all the years in IPL

2. Number of matches won per team per year in IPL

3. Extra runs conceded per team in the year 2016

4. Top 10 economical bowlers in the year 2015

5. Number of times each team won the toss and also won the match

6. Player who has won the highest number of Player of the Match awards for each season

7. Strike rate of a batsman for each season

8. Highest number of times one player has been dismissed by another player

9. Bowler with the best economy in super overs


*The reports can be found in "./src/public/", available in json format.*

<br>

## Data Source

<br>

[IPL Dataset](https://www.kaggle.com/datasets/manasgarg/ipl)

<br>

## Prerequisites

<br>

- Install Node.js

    ```bash
    sudo apt install nodejs
    ```

<br>

## Installation

<br>

- Fork/Download the project

- To execute respective files:

    ```bash
    node 1-matches-per-year.js
    node 2-matches-won-per-team-per-year.js
    node 3-extra-runs-conceded-per-team-in-2016.js
    node 4-top-10-economical-bowlers-in-2015.js
    node 5-number-of-times-each-team-won-toss-and-the-match.js
    node 6-players-who-won-the-highest-number-of-player-of-the-match-for-each-season.js
    node 7-strike-rate-of-batsman-for-each-season.js
    node 8-highest-number-of-times-one-player-has-been-dismissed-by-another-player.js
    node 9-bowler-with-the-best-economy-in-super-overs.js
    ```

<br>

## Credits

<br>

[Sagar Dutta](https://github.com/sd704/)